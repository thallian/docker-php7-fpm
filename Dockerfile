FROM registry.gitlab.com/thallian/docker-confd-env:master

RUN apk --no-cache add php7-fpm

ADD /rootfs /
