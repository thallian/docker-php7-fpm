[php-fpm](https://php-fpm.org/) with PHP7.

# Environment Variables
## FPMUSER
- default: nobody

The user which runs the fpm process.

## FPMGROUP
- default: nobody

The group which runs the fpm process.

## MAX_UPLOAD_SIZE
- default: 512M

Maximal upload size.

## PHP_MEMORY_LIMIT
- default: 128M

Maximum amount of memory that a script is allowed to allocate.

## PHP_MAX_EXECUTION_TIME
- default: 30

Execution time limit for php scripts in seconds.

## PHP_MAX_CHILDREN
- default: 5

Value of `pm.max_children`.

## PHP_START_SERVERS
- default: 2

Value of `pm.start_servers`

## PHP_MAX_SPARE_SERVERS
- default: Value of PHP_START_SERVERS or 3

Value of `pm.max_spare_servers`

## PHP_ADMIN_VALUES
Comma seperated list of php admin values.

Each element is a key-value pair, seperated by an equal sign.


## PHP_ADMIN_FLAGS
Comma seperated list of php admin flags.

Each element is a key-value pair, seperated by an equal sign.
